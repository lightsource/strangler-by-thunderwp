# StranglerByThunderWP

## What is it?

An example of a [mu-plugin](https://wordpress.org/documentation/article/must-use-plugins/) for WordPress that disables
the parent's theme
and some plugins for specific requests ONLY.

It's useful in the case of the 'child' mode in the ThunderWP framework.

## How to install

Add the php file to the `wp-content/mu-plugins` folder of your WP installation.