<?php
/*
Plugin Name: Strangler by ThunderWP
Description: Disables unused plugins and parent's theme for specific pages
Version: 1.0.0
*/

class StranglerByThunderWP
{
    protected function isTargetPage(): bool
    {
        if (is_admin()) {
            return false;
        }

        $requestUrl = $_SERVER['REQUEST_URI'] ?? '';

        // todo add your checks

        return false;
    }

    protected function getUnusedPluginSlugs(): array
    {
        return [
            // todo add yours
        ];
    }

    public function setHooks(): void
    {
        add_filter('template_directory', [$this, 'maybeReplaceTheParentThemeDir',]);
        add_filter('option_active_plugins', [$this, 'maybeRemoveUnusedPlugins',]);
    }

    public function maybeReplaceTheParentThemeDir($templateDir): string
    {
        if (!$this->isTargetPage()) {
            return $templateDir;
        }

        return get_stylesheet_directory();
    }

    public function maybeRemoveUnusedPlugins($activePlugins): array
    {
        if (!is_array($activePlugins) ||
            !$this->isTargetPage()) {
            return $activePlugins;
        }

        $activePlugins = array_diff($activePlugins, $this->getUnusedPluginSlugs());

        return array_values($activePlugins);
    }
}

(new StranglerByThunderWP())->setHooks();
